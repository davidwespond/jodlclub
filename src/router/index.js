import Vue from "vue";
import VueRouter from "vue-router";
import MostLiked from "../views/MostLiked.vue";

Vue.use(VueRouter);

const routes = [
  { path: '/', redirect: '/MostLiked' }, // redirect to mostliked-Page
  {
    path: "/MostLiked",
    name: "MostLiked",
    component: MostLiked
  },
  {
    path: "/MostComments",
    name: "MostComments",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/MostComments.vue")
  },
  {
    path: "/Newest",
    name: "Newest",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Newest.vue")
  }
];

const router = new VueRouter({
  routes
});

export default router;
