import Vue from "vue";
import Vuex from "vuex";
import sourceData from "@/data.json";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    sourceData,
    count: 0
  },
  mutations: {
    increment (state) {
      state.count++;
    },
    addNewJodl (state, jodlStore, jodlID, jodl){
      this.$set(jodlStore, jodlID, jodl)
    }
  },
  actions: {},
  modules: {}
});
